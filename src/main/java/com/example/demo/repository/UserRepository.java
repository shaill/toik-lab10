package com.example.demo.repository;

import com.example.demo.model.User;
import com.example.demo.rest.UserApiController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

public class UserRepository {

    private final Map<Integer, User> usersDatabase;
    public enum Code{
        OK,UNAUTHORIZED,FORBIDDEN
    }
    public UserRepository() {
        usersDatabase = new HashMap<>();

        usersDatabase.put(1, new User("cracker", "cracker1234", true, 0));
        usersDatabase.put(2, new User("marry", "marietta!#09", true, 0));
        usersDatabase.put(3, new User("silver", "$silver$", true, 0));
    }

    public Code checkLogin(final String login, final String password) {
        for (Map.Entry<Integer,User> a: usersDatabase.entrySet())
        {
            User user = a.getValue ();
            if (user.getLogin().equals(login)) {
                if(user.isActive()) {
                    if (user.getPassword().equals(password))
                        //200
                        return Code.OK;
                    else {
                        user.setIncorrectLoginCounter(user.getIncorrectLoginCounter() + 1);
                        if(user.getIncorrectLoginCounter()==3){
                            user.setActive(false);
                        }
                        //401
                        return Code.UNAUTHORIZED;
                    }
                }
                //403
                return Code.FORBIDDEN;
            }
        }
        //401
        return Code.UNAUTHORIZED;
    }
}
